SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Gatewayuniquecode] (
		[Company_Name]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[company_code]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Unique_code]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Account]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Gatewayuniquecode] SET (LOCK_ESCALATION = TABLE)
GO
