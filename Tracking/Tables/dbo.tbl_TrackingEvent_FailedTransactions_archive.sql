SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_FailedTransactions_archive] (
		[RowID]                 [int] IDENTITY(1, 1) NOT NULL,
		[TrackingEventJSON]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ErrorNumber]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorSeverity]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorState]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorLine]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorProcedure]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorMessage]          [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]           [bit] NOT NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_TrackingEvent_FailedTransactions]
		PRIMARY KEY
		CLUSTERED
		([RowID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_FailedTransactions_archive]
	ADD
	CONSTRAINT [DF_tbl_TrackingEvent_FailedTransactions_IsProcessed]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_FailedTransactions_archive] SET (LOCK_ESCALATION = TABLE)
GO
