SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[companies-2021-07-12] (
		[Company_Name]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Billing_Group]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Accounts]                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EzyFreight_Pricecodes]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Zone_Map]                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Default_Pricecode]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CPPL_Code]                 [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Unique_Code]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Active]                    [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[companies-2021-07-12] SET (LOCK_ESCALATION = TABLE)
GO
