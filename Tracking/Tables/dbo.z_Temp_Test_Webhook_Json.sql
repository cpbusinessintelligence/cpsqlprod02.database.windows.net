SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[z_Temp_Test_Webhook_Json] (
		[id]           [int] IDENTITY(1, 1) NOT NULL,
		[JsonFile]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[z_Temp_Test_Webhook_Json] SET (LOCK_ESCALATION = TABLE)
GO
