SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_ProntoImport_Backup_25032019] (
		[TrackingEventID]               [int] IDENTITY(1, 1) NOT NULL,
		[WorkflowType]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Outcome]                       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentNumber]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LabelNumber]                   [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Relation]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EventDateTime]                 [datetime] NOT NULL,
		[DriverRef]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsSuccessful]                  [bit] NULL,
		[NowGoWorkflowCompletionID]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoConsignmentID]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessedCPNImport]          [bit] NULL,
		[IsProcessedSLSImport]          [bit] NULL,
		[CreatedDateTime]               [datetime] NOT NULL,
		[UpdatedDateTime]               [datetime] NULL
)
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_ProntoImport_Backup_25032019] SET (LOCK_ESCALATION = TABLE)
GO
