SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_trackingevent_agent_Errors] (
		[ErrorID]              [int] IDENTITY(1, 1) NOT NULL,
		[ErrorPackageName]     [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorDesc]            [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDate]          [datetime] NULL
)
GO
ALTER TABLE [dbo].[tbl_trackingevent_agent_Errors]
	ADD
	CONSTRAINT [DF__tbl_track__Creat__1C873BEC]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tbl_trackingevent_agent_Errors] SET (LOCK_ESCALATION = TABLE)
GO
