SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_Backup_25032019] (
		[EventID]                          [int] IDENTITY(1, 1) NOT NULL,
		[WorkflowType]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Outcome]                          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Attribute]                        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AttributeValue]                   [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EventCode]                        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentNumber]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LabelNumber]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Relation]                         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ItemCount]                        [int] NULL,
		[EventDateTime]                    [datetime] NOT NULL,
		[EventDateTimeUTC]                 [datetime] NOT NULL,
		[DriverId]                         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeviceId]                         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]                           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Latitude]                         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Longitude]                        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DLBCode]                          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RedeliveryBarCode]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RedeliveryBarCodeType]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReferenceBarcode1]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReferenceBarcode2]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SigneeName]                       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsSuccessful]                     [bit] NULL,
		[NowGoWorkflowCompletionID]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoConsignmentID]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoSubjectArticleID]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SubjectArticleCustomData]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WorkflowCompletionCustomData]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EvidenceCustomData]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]                      [bit] NULL,
		[CreatedDateTime]                  [datetime] NOT NULL,
		[CreatedBy]                        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]                  [datetime] NULL,
		[UpdatedBy]                        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EventJSON]                        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_Backup_25032019] SET (LOCK_ESCALATION = TABLE)
GO
