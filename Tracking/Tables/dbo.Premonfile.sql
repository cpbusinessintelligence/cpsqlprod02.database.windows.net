SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Premonfile] (
		[ID]                    [int] NULL,
		[Body]                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[url]                   [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Permanentlyfailed]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Createdat]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Premonfile] SET (LOCK_ESCALATION = TABLE)
GO
