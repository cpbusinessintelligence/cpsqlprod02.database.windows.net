SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_EventDefinition] (
		[EventDefID]           [int] IDENTITY(1, 1) NOT NULL,
		[EventCode]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[EventCategory]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[EventDescription]     [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TrackingDisplay]      [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DisplayCategory]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EventOriginated]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[HasImage]             [bit] NULL,
		[DLBAvalilable]        [bit] NULL,
		[PODAvalilable]        [bit] NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_TrackingEvent_EventDefinition]
		PRIMARY KEY
		CLUSTERED
		([EventDefID])
)
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_EventDefinition] SET (LOCK_ESCALATION = TABLE)
GO
