SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_Agent_IncomingJSON] (
		[RowID]               [int] IDENTITY(1, 1) NOT NULL,
		[EventJSON]           [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AgentName]           [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]         [bit] NULL,
		[CreatedDateTime]     [datetime] NULL,
		CONSTRAINT [PK_tbl_TrackingEvent_IncomingJSON_Agent1]
		PRIMARY KEY
		CLUSTERED
		([RowID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_Agent_IncomingJSON]
	ADD
	CONSTRAINT [DF__tbl_Track__IsPro__0F2D40CE]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_Agent_IncomingJSON]
	ADD
	CONSTRAINT [DF__tbl_Track__Creat__10216507]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_Agent_IncomingJSON] SET (LOCK_ESCALATION = TABLE)
GO
