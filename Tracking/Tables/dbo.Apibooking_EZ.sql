SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Apibooking_EZ] (
		[Id]                   [int] NOT NULL,
		[Token]                [int] NOT NULL,
		[Status]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Message]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Request]              [nvarchar](600) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[RequestUrl]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Response]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[InternalLogs]         [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[API]                  [int] NOT NULL,
		[CreatedDateTime]      [datetime2](7) NOT NULL,
		[Sandbox]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Id2]                  [int] NOT NULL,
		[Account]              [int] NOT NULL,
		[Description]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Token2]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Sandbox2]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Active]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CreatedDateTime2]     [datetime2](7) NOT NULL,
		[Id3]                  [int] NOT NULL,
		[Name]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CreatedBy]            [int] NOT NULL,
		[CreatedDate]          [datetime2](7) NOT NULL,
		[HourlyLimit]          [int] NOT NULL,
		[DailyLimit]           [int] NOT NULL,
		[ActiveTest]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ActiveProduction]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CpAccount]            [int] NOT NULL
)
GO
ALTER TABLE [dbo].[Apibooking_EZ] SET (LOCK_ESCALATION = TABLE)
GO
