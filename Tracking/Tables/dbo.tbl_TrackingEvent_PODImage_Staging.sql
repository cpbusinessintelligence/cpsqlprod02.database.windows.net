SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_PODImage_Staging] (
		[RowID]               [int] IDENTITY(1, 1) NOT NULL,
		[PODImageID]          [int] NOT NULL,
		[IsDownloaded]        [bit] NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_TrackingEvent_PODImage_Staging]
		PRIMARY KEY
		CLUSTERED
		([RowID])
)
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_PODImage_Staging]
	ADD
	CONSTRAINT [DF_tbl_TrackingEvent_PODImage_Staging_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_PODImage_Staging] SET (LOCK_ESCALATION = TABLE)
GO
