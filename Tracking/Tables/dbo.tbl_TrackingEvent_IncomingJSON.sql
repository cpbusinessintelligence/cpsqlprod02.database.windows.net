SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_IncomingJSON] (
		[RowID]               [int] IDENTITY(1, 1) NOT NULL,
		[EventID]             [int] NULL,
		[EventJSON]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NULL,
		CONSTRAINT [PK_tbl_TrackingEvent_IncomingJSON]
		PRIMARY KEY
		CLUSTERED
		([RowID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_IncomingJSON] SET (LOCK_ESCALATION = TABLE)
GO
