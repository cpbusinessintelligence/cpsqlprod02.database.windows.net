SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_Integration_Staging] (
		[RowId]                         [int] IDENTITY(1, 1) NOT NULL,
		[EventID]                       [int] NULL,
		[TrackingNumber]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentNumber]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WorkflowType]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Outcome]                       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EventDateTime]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ScanEvent]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverRunNumber]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]                        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ExceptionReason]               [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AlternateBarcode]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RedeliveryCard]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DLB]                           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[URL]                           [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PODName]                       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PODImageId]                    [int] NULL,
		[ImageType]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoWorkflowCompletionID]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PODImageURL]                   [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoConsignmentID]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoSubjectArticleID]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BookingID]                     [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_TrackingEvent_Integration_Staging]
		PRIMARY KEY
		CLUSTERED
		([RowId])
)
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_Integration_Staging] SET (LOCK_ESCALATION = TABLE)
GO
