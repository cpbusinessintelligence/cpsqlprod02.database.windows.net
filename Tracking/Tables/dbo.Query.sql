SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Query] (
		[Id]                  [int] NOT NULL,
		[Token]               [int] NULL,
		[Status]              [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Message]             [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Request]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RequestUrl]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Response]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[InternalLogs]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[API]                 [int] NULL,
		[CreatedDateTime]     [datetimeoffset](7) NOT NULL,
		[Sandbox]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Query] SET (LOCK_ESCALATION = TABLE)
GO
