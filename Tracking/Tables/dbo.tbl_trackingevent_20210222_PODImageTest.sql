SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_trackingevent_20210222_PODImageTest] (
		[EventID]                       [int] NOT NULL,
		[ConsignmentNumber]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LabelNumber]                   [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[WorkflowType]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Outcome]                       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Attribute]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AttributeValue]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Relation]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ItemCount]                     [int] NULL,
		[DriverId]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverExtRef]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverRunNumber]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]                        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeviceId]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Latitude]                      [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Longitude]                     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SigneeName]                    [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsSuccessful]                  [bit] NULL,
		[NowGoWorkflowCompletionID]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoConsignmentID]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoSubjectArticleID]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]                   [bit] NULL,
		[EventDateTime]                 [datetime] NOT NULL,
		[CreatedDateTime]               [datetime] NOT NULL,
		[RecipientIdType]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RecipientDateOfBirth]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RecipientAge]                  [int] NULL
)
GO
ALTER TABLE [dbo].[tbl_trackingevent_20210222_PODImageTest] SET (LOCK_ESCALATION = TABLE)
GO
