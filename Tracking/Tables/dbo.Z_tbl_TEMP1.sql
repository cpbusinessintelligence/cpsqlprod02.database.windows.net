SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Z_tbl_TEMP1] (
		[EventID]                       [varchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[TrackingNumber]                [varchar](17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ConsignmentNumber]             [int] NULL,
		[WorkflowType]                  [varchar](7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Outcome]                       [varchar](9) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[EventDateTime]                 [varchar](19) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ScanEvent]                     [varchar](9) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DriverRunNumber]               [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Branch]                        [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ExceptionReason]               [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AlternateBarcode]              [int] NULL,
		[RedeliveryCard]                [int] NULL,
		[DLB]                           [int] NULL,
		[URL]                           [varchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[PODName]                       [int] NULL,
		[PODImageID]                    [int] NULL,
		[ImageType]                     [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[NowGoWorkflowCompletionID]     [varchar](18) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[NowGoConsignmentID]            [varchar](18) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[NowGoSubjectArticleID]         [varchar](18) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Z_tbl_TEMP1] SET (LOCK_ESCALATION = TABLE)
GO
