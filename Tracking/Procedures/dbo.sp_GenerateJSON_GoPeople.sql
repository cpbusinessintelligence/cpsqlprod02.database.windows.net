SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Proc [dbo].[sp_GenerateJSON_GoPeople]

As
Begin
Declare @Json varchar(5000)
  select @Json = EventJson from [dbo].[tbl_TrackingEvent_Agent_IncomingJSON] where isprocessed = 0

 Insert into dbo.tbl_trackingevent_agent (TrackingNumber,ConsignmentNumber,EventDateTime,ScanEvent,DriverRunNumber,Branch,ExceptionReason,AlternateBarcode,RedeliveryCard,DLB,URL,PODName,PODImageURL)
SELECT TrackingNumber,ConsignmentNumber,EventDateTime,Lower(ScanEvent) as ScanEvent,DriverRunNumber,Branch,ExceptionReason,AlternateBarcode,RedeliveryCard,DLB,URL,PODName,PODImageURL FROM OPENJSON(@Json)
WITH(TrackingNumber varchar(100) '$.TrackingNumber',
ConsignmentNumber varchar(100) '$.ConsignmentNumber',
EventDateTime varchar(100) '$.EventDateTime',
ScanEvent varchar(100) '$.ScanEvent',
DriverRunNumber varchar(100) '$.DriverRunNumber',

Branch varchar(100) '$.Branch',
ExceptionReason varchar(100) '$.ExceptionReason',
AlternateBarcode varchar(100) '$.AlternateBarcode',
RedeliveryCard varchar(100) '$.RedeliveryCard',
DLB varchar(100) '$.DLB',
URL varchar(100) '$.URL',
PODName varchar(100) '$.PODName',
PODImageURL varchar(1000) '$.PODImageURL'
)

update [dbo].[tbl_TrackingEvent_Agent_IncomingJSON]
set isprocessed = 1
where isprocessed = 0


update dbo.tbl_trackingevent_agent
set Driverrunnumber = 9407
where isprocessed = 0 

update dbo.tbl_trackingevent_agent
set Branch  = 'sydney'
where isprocessed = 0 

update dbo.tbl_trackingevent_agent
set ScanEvent  = Lower(ScanEvent)
where isprocessed = 0 

UPDATE dbo.tbl_trackingevent_agent 
SET EventDateTime=LEFT(EventDateTime, LEN(EventDateTime)-5)
where isprocessed = 0 

UPDATE dbo.tbl_trackingevent_agent 
SET EventDateTime=CONVERT(DATETIME,convert(datetime,EventDateTime) AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time')
where isprocessed = 0 


Select
(
select TrackingNumber,ConsignmentNumber,EventDateTime,Lower(ScanEvent) as ScanEvent,DriverRunNumber,Branch,ExceptionReason,AlternateBarcode,RedeliveryCard,DLB,URL,PODName,PODImageURL  
from dbo.tbl_trackingevent_agent 
where isprocessed = 0 
FOR JSON AUTO
) as JSONSTring

End
GO
