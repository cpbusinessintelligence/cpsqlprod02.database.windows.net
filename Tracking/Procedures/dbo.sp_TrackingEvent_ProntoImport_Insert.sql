SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_TrackingEvent_ProntoImport_Insert]
(
        @JSON NVARCHAR(MAX)
)
AS
BEGIN
    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRY
		
		DECLARE @WorkflowType VARCHAR(50) = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.workflow_type'));
		DECLARE @Outcome VARCHAR(50) = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.outcome'));
		
		IF (@WorkflowType = 'deliver' OR @WorkflowType = 'pickup')
		BEGIN
			INSERT INTO [dbo].[tbl_TrackingEvent_ProntoImport]
				([WorkflowType]
				,[Outcome]
				,[ConsignmentNumber]
				,[LabelNumber]
				,[Relation]
				,[EventDateTime]
				,[DriverRef]
				,[IsSuccessful]
				,[NowGoWorkflowCompletionID]
				,[NowGoConsignmentID]
				,[CreatedDateTime])
			SELECT		
				JSON_VALUE(@JSON, '$.message_data.workflow_completion.workflow_type') AS [WorkflowType],
				JSON_VALUE(@JSON, '$.message_data.workflow_completion.outcome') AS [Outcome],
				NULL AS [ConsignmentNumber],
				JSON_VALUE (IDENTIFIERS.value, '$.identifier') AS [LabelNumber],
				JSON_VALUE (IDENTIFIERS.value, '$.relation') AS [Relation],
				CONVERT(DATETIME, CONVERT(DATETIME2, JSON_VALUE(@JSON, '$.message_data.workflow_completion.time'))  AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS [EventDateTime],
				JSON_VALUE(@JSON, '$.message_data.workflow_completion.driver_ext_ref') AS [DriverRef],
				JSON_VALUE(@JSON, '$.message_data.workflow_completion.is_successful') AS [IsSuccessful],
				JSON_VALUE(@JSON, '$.message_data.workflow_completion.id') AS [NowGoWorkflowCompletionID],
				JSON_VALUE (ARTICLES.value, '$.consignment_id') AS [NowGoConsignmentID],
				CONVERT(DATETIME, GETDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS CreatedDateTime
			FROM OPENJSON (@json, '$.message_data.workflow_completion.subject_articles') AS ARTICLES
			CROSS APPLY OPENJSON (ARTICLES.value, '$.identifiers') AS IDENTIFIERS;
		END
		
	END TRY

	BEGIN CATCH

		THROW;

	END CATCH	
END
GO
