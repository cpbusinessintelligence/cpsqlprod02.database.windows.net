SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_TrackingIntegration_UpdateIsProcessedFlag]
(
	@Lower INT,
	@Upper INT
)
AS
BEGIN

    SET NOCOUNT ON

	UPDATE TRK
		SET TRK.IsProcessed = 1
	FROM tbl_TrackingEvent TRK
	INNER JOIN
	tbl_TrackingEvent_Integration_Staging STG
	ON TRK.EventID = STG.EventID
	WHERE STG.[RowId] BETWEEN @Lower AND @Upper		
END


GO
