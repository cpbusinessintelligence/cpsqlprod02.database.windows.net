SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



 
CREATE PROCEDURE [dbo].[sp_TrackingEvent_WebHooks_Get_AccessToken]
AS
BEGIN
    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	Select Message_Id,Access_Token From [tbl_TrackingEvent_WebHooks_AccessData] Where IsProcessed = 0

END
GO
