SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO







--exec sp_WebHooks_AccessData_Insert '{"message_type":"claim_check","message_data":{"message_id":"d646a08a-a7f9-4d01-add6-2204c50fc757","access_token":"vai1ar2Olai1EegaSie8maez","available_until":"2021-01-01T12:34:56Z"}}'

--exec sp_WebHooks_AccessData_Insert '{"message_type": "workflow_completion.create","message_data": {"workflow_completion": {}}}'

CREATE PROCEDURE [dbo].[sp_TrackingEvent_WebHooks_AccessToken_Insert]
(
	@JSON NVARCHAR(MAX)
)
AS
BEGIN
    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRAN

	BEGIN TRY


		DECLARE @IMGTABLE TABLE (
			[EventID] INT, 
			[LabelNumber] VARCHAR(100)
		);
		
		DECLARE @MessageType VARCHAR(50) = (SELECT * FROM OPENJSON(@JSON) WITH (message_type varchar(50) '$.message_type'));
	
		IF @MessageType = 'claim_check' 
		BEGIN
			DECLARE @WorkflowType VARCHAR(50) = (SELECT JSON_VALUE(@JSON, '$.message_data.message_id'));
			DECLARE @Outcome VARCHAR(50) = (SELECT JSON_VALUE(@JSON, '$.message_data.access_token'));
		
			INSERT INTO [dbo].[tbl_TrackingEvent_WebHooks_AccessData]
				   ([Message_Id] 
				   ,[Access_Token]
				   ,[CreatedDateTime]
					)
			SELECT		
				@WorkflowType,
				@Outcome,
				GETDATE()
		END
		ELSE
		BEGIN
			EXEC sp_TrackingEvent_Insert_WebhookData @JSON, 'N'
		END

	END TRY

	BEGIN CATCH

		IF @@TRANCOUNT >0
		BEGIN		
				ROLLBACK TRAN;
				INSERT INTO [tbl_TrackingEvent_WebHooks_AccessData_FailedTransactions]
					([AccessDataJSON]
					,[ErrorNumber]
					,[ErrorSeverity]
					,[ErrorState]
					,[ErrorLine]
					,[ErrorProcedure]
					,[ErrorMessage]
					,[CreatedDateTime])
					SELECT
					 @JSON
					,ERROR_NUMBER() AS ErrorNumber  
					,ERROR_SEVERITY() AS ErrorSeverity  
					,ERROR_STATE() AS ErrorState  
					,ERROR_LINE () AS ErrorLine  
					,ERROR_PROCEDURE() AS ErrorProcedure  
					,ERROR_MESSAGE() AS ErrorMessage
					,CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS CreatedDateTime;
		END;

		THROW;

	END CATCH

	IF @@TRANCOUNT >0
	BEGIN	
		COMMIT TRAN;
	END
END
GO
