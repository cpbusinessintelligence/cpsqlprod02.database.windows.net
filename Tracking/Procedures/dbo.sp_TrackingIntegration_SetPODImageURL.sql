SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



--[sp_TrackingIntegration_SetPODImageURL] 'https://cpprodnowgostorage1.blob.core.windows.net/trackingpod/','?sv=2018-03-28&si=trackingpod-16AA007FC78&sr=c&sig=ZF4x%2B2Tkl5NbyWQqQF8NM34pW3R7wwf9ul0gTJ9LpyI%3D'
CREATE PROCEDURE [dbo].[sp_TrackingIntegration_SetPODImageURL]
(
	@BlobFolderEnpoint VARCHAR(100),
	@BlobFolderSharedAccessSignature VARCHAR(200)
)
AS
BEGIN
    SET NOCOUNT ON

	UPDATE [tbl_TrackingEvent_Integration_Staging]
	SET [PODImageURL] = CASE WHEN ImageType IS NOT NULL THEN @BlobFolderEnpoint + CONVERT(VARCHAR, PODImageID) + [dbo].[fn_GET_IMAGE_EXTN] (ImageType) + @BlobFolderSharedAccessSignature ELSE NULL END;	 

	UPDATE [tbl_TrackingEvent_Integration_Staging]
	SET [URL] = CASE WHEN ImageType IS NOT NULL THEN @BlobFolderEnpoint + CONVERT(VARCHAR, [URL]) + [dbo].[fn_GET_IMAGE_EXTN] (ImageType) + @BlobFolderSharedAccessSignature ELSE NULL END;	 	
	
END
GO
