SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_TrackingEvent_Parse]
(
	@JSON NVARCHAR(MAX)
)
AS
BEGIN
    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRAN

	BEGIN TRY

		DECLARE @IMGTABLE TABLE (
			[EventID] INT, 
			[LabelNumber] VARCHAR(100)
		);

		DECLARE @WorkflowType VARCHAR(50) = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.workflow_type'));
		DECLARE @Outcome VARCHAR(50) = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.outcome'));
		DECLARE @DriverExtRef VARCHAR(50) = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.driver_ext_ref'));
		DECLARE @Attribute VARCHAR(50) = NULL;
		DECLARE @AttributeValue VARCHAR(100) = NULL;
		
		SELECT @Attribute = Attribute, @AttributeValue = AttributeValue FROM [fn_GET_ATTRIBUTES](@JSON, @WorkflowType, @Outcome);

		INSERT INTO [dbo].[tbl_TrackingEvent]
			   ([WorkflowType]
			   ,[Outcome]
			   ,[Attribute]
			   ,[AttributeValue]
			   ,[ConsignmentNumber]
			   ,[LabelNumber]
			   ,[Relation]
			   ,[ItemCount]
			   ,[EventDateTime]
			   ,[DriverId]
			   ,[DriverExtRef]
			   ,[DriverRunNumber]
			   ,[Branch]
			   ,[DeviceId]			   
			   ,[Latitude]
			   ,[Longitude]			   
			   ,[SigneeName]
			   ,[IsSuccessful]
			   ,[NowGoWorkflowCompletionID]
			   ,[NowGoConsignmentID]
			   ,[NowGoSubjectArticleID]
			   ,[CreatedDateTime])
		OUTPUT	INSERTED.[EventID],
				INSERTED.[LabelNumber] 
		INTO @IMGTABLE (
				[EventID],
				[LabelNumber]
			)
		SELECT		
			@WorkflowType,
			@Outcome,
			@Attribute,
			@AttributeValue,
			NULL AS [ConsignmentNumber],
			JSON_VALUE (IDENTIFIERS.value, '$.identifier') AS [LabelNumber],
			JSON_VALUE (IDENTIFIERS.value, '$.relation') AS [Relation],
			JSON_VALUE (ARTICLES.value, '$.item_count') AS [ItemCount],
			[dbo].[fn_GET_DATETIME_BY_BRANCH](@DriverExtRef, JSON_VALUE(@JSON, '$.message_data.workflow_completion.time')) AS [EventDateTime],
			JSON_VALUE(@JSON, '$.message_data.workflow_completion.driver_id') AS [DriverId],
			@DriverExtRef AS [DriverExtRef],
			[dbo].[fn_GET_DRIVER_RUN_NUMBER](@DriverExtRef) AS [DriverRunNumber],
			[dbo].[fn_GET_BRANCH](@DriverExtRef) AS [Branch],
			JSON_VALUE(@JSON, '$.message_data.workflow_completion.device_id') AS [DeviceId],
			JSON_VALUE(@JSON, '$.message_data.workflow_completion.latitude') AS [Latitude],
			JSON_VALUE(@JSON, '$.message_data.workflow_completion.longitude') AS [Longitude],
			JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.signee_name') AS [SigneeName],
			JSON_VALUE(@JSON, '$.message_data.workflow_completion.is_successful') AS [IsSuccessful],
			JSON_VALUE(@JSON, '$.message_data.workflow_completion.id') AS [NowGoWorkflowCompletionID],
			JSON_VALUE (ARTICLES.value, '$.consignment_id') AS [NowGoConsignmentID],
			JSON_VALUE (ARTICLES.value, '$.id') AS [NowGoSubjectArticleID],
			CONVERT(DATETIME, GETDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS CreatedDateTime
		FROM OPENJSON (@json, '$.message_data.workflow_completion.subject_articles') AS ARTICLES
		CROSS APPLY OPENJSON (ARTICLES.value, '$.identifiers') AS IDENTIFIERS;
		
		IF @WorkflowType = 'deliver' AND @Outcome = 'delivered'
		BEGIN
			INSERT INTO [dbo].[tbl_TrackingEvent_PODImage]
				   ([EventID]
				   ,[LabelNumber]
				   ,[NowGoImageID]
				   ,[ImageType]
				   ,[ImageDetail]
				   ,[IsDownloaded]
				   ,[CreatedDateTime])
			SELECT	
				[EventID],
				[LabelNumber],
				ISNULL(JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.signature_image.id'), JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.attachments[0].id')) AS [NowGoImageID],
				ISNULL(JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.signature_image.media_type'), JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.attachments[0].media_type')) AS [ImageType],
				ISNULL(JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.signature_image.label'), JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.attachments[0].label')) AS [ImageDetail],
				0 AS [IsDownloaded],
				CONVERT(DATETIME, GETDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS CreatedDateTime
			FROM @IMGTABLE
		END
		
		INSERT INTO [dbo].[tbl_TrackingEvent_IncomingJSON]
				   ([EventID]
				   ,[EventJSON]
				   ,[CreatedDateTime])
		SELECT	
				[EventID],
				@JSON,
				CONVERT(DATETIME, GETDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time')
		FROM @IMGTABLE

		EXEC [dbo].[sp_TrackingEvent_ProntoImport_Insert] @JSON = @JSON;
	END TRY

	BEGIN CATCH

		IF @@TRANCOUNT >0
		BEGIN		
				ROLLBACK TRAN;
				INSERT INTO [tbl_TrackingEvent_FailedTransactions]
					([TrackingEventJSON]
					,[ErrorNumber]
					,[ErrorSeverity]
					,[ErrorState]
					,[ErrorLine]
					,[ErrorProcedure]
					,[ErrorMessage]
					,[CreatedDateTime])
					SELECT
					 @JSON
					,ERROR_NUMBER() AS ErrorNumber  
					,ERROR_SEVERITY() AS ErrorSeverity  
					,ERROR_STATE() AS ErrorState  
					,ERROR_LINE () AS ErrorLine  
					,ERROR_PROCEDURE() AS ErrorProcedure  
					,ERROR_MESSAGE() AS ErrorMessage
					,CONVERT(DATETIME, GETDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS CreatedDateTime;
		END;

		THROW;

	END CATCH

	IF @@TRANCOUNT >0
	BEGIN	
		COMMIT TRAN;
	END
END
GO
