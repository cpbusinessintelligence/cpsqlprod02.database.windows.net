SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_TrackingEvent_Insert_Failed_Transactions]
(
	@json NTEXT = NULL,
	@exception_stack_trace VARCHAR(500) = NULL
)
AS
BEGIN

    SET NOCOUNT ON

    INSERT INTO [dbo].tbl_TrackingEvent_FailedTransactions
           ([TrackingEventJSON]
           ,[ErrorMessage]
           ,[IsProcessed]
           ,[CreatedDateTime])
     VALUES
           (@json
           ,@exception_stack_trace
           ,0
           ,CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time'))
		
END
GO
