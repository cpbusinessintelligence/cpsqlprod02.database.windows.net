SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO











CREATE PROCEDURE [dbo].[sp_TrackingIntegration_GetTrackingEvents]

AS
BEGIN
/*
Change History
-----------------------------------------------------------------------------
CH #153 - Anand - 12/06/2019 - Added condition when Workflowtype is Transfer and populated the AttributeValue when it is a DLB
CH #158 - Anand - 18/06/2019 - Changed when the outcome = onboard from in-transit to onboard
								Added NowGoConsignmentID and NowGoSubjectArticleID columns to be inserted into tbl_Tracking_Integration_Staging
CH #168 - Anand - 21/06/2019 - Changed ATL with Photo message to just display as ATL
CH #184 - Anand - 24/07/2019 - Changed Display text - Card left-in vehicle TO Card Left - In Vehicle AND Card left-unsafe to leave TO	Card Left - Unsafe to Leave
								Changed AttributeValues when workflowtype is PICKUP and outcome is failed-goods-not-ready OR failed-no-goods-to-go 
								failed-unable-to-transport OR failed-closed OR failed-no-access-available OR failed-pickup
CH # 229 - Anand - 17/01/2020 - Changed Display Text to match what Gateway displays when there is a futile delivery.
CH # 324   RAj Marikant 23/10/2020 - Achohol delivery Changes.   
			Anand - 28/10/2020  
			Anand - 03/11/2020 - Added Failed-Defer changes

CH # 439   Anand 08/07/2021 - Failed Achohol delivery Changes.   
			
*/


  SET NOCOUNT ON

  
		--Select '77046716' [EventID],'CPBBGRZ6066691001' [TrackingNumber],NULL [ConsignmentNumber],'deliver' [WorkflowType],'delivered' [Outcome],'2020-12-17 13:23:00'[EventDateTime],'delivered' [ScanEvent],'6098'[DriverRunNumber],'perth'[Branch],'ATL'[ExceptionReason],	NULL[AlternateBarcode],	NULL[RedeliveryCard],	NULL[DLB],	'33860570'[URL],	NULL[PODName],	NULL[PODImageID],	'image/jpeg' [ImageType],	'391822747551926244'[NowGoWorkflowCompletionID],'388156604488355373'[NowGoConsignmentID],'388156604521910259' [NowGoSubjectArticleID]
		--INTO Z_tbl_TEMP1
		--Union
		--Select '77046716' [EventID],'CPBBGRZ6066691001' [TrackingNumber],NULL [ConsignmentNumber],'deliver' [WorkflowType],'delivered' [Outcome],'2020-12-17 13:23:00'[EventDateTime],'delivered' [ScanEvent],'6098'[DriverRunNumber],'perth'[Branch],'ATL'[ExceptionReason],	NULL[AlternateBarcode],	NULL[RedeliveryCard],	NULL[DLB],	'33860571'[URL],	NULL[PODName],	NULL[PODImageID],	'image/jpeg' [ImageType],	'391822747551926244'[NowGoWorkflowCompletionID],'388156604488355373'[NowGoConsignmentID],'388156604521910259' [NowGoSubjectArticleID]


  SELECT DISTINCT * FROM 
  (
		
		--SELECT * FROM Z_tbl_TEMP1
		--UNION
		-- Non Delivery Tracking Events
		SELECT
			EventID,
			[Labelnumber] AS [TrackingNumber], 
			NULL AS [ConsignmentNumber],
			[WorkflowType],
			[Outcome],
			CONVERT(VARCHAR, EventDatetime, 120) AS [EventDateTime], 
			CASE 	
			-- Changed from in-transit to onbard on 18/06/2019. will probably need to revisit once NowGo is released to other
				WHEN Outcome = 'onboard' THEN 'onboard'
				--CH #184
				WHEN Outcome = 'failed-pickup' THEN 'pickup'
				WHEN Outcome = 'failed-no-access-available' THEN 'pickup'
				WHEN Outcome = 'failed-closed' THEN 'pickup'
				WHEN Outcome = 'failed-unable-to-transport' THEN 'pickup'
				WHEN Outcome = 'failed-no-goods-to-go' THEN 'pickup'
				WHEN Outcome = 'failed-goods-not-ready' THEN 'pickup'
				WHEN Outcome = 'failed-defer' THEN ''
				ELSE Outcome
				END 
			AS ScanEvent, 
			--Change made by AH - 21/04/2020 for DeliverE Agent only
			CASE WHEN rtrim(ltrim(driverextref))= 'delivere-delegate-driver' THEN '9407' ELSE [DriverRunNumber] END as [DriverRunNumber],
			CASE WHEN rtrim(ltrim(driverextref))= 'delivere-delegate-driver' THEN 'sydney' ELSE [Branch] END as [Branch],
			CASE WHEN WorkflowType = 'offboard' AND Outcome = 'offboard-popshop' THEN Attribute + IIF(AttributeValue IS NULL,'', '. Barcode - ' + AttributeValue) 
				 WHEN WorkflowType = 'offboard' AND Outcome = 'offboard-popstation' THEN Attribute + IIF(AttributeValue IS NULL,'', '. Barcode - ' + AttributeValue) 

				 --CH #184
				 WHEN WorkflowType = 'pickup' AND Outcome = 'failed-pickup' THEN AttributeValue
				 WHEN WorkflowType = 'pickup' AND Outcome = 'failed-no-access-available' THEN AttributeValue 
				 WHEN WorkflowType = 'pickup' AND Outcome = 'failed-closed' THEN AttributeValue  			 
				 WHEN WorkflowType = 'pickup' AND Outcome = 'failed-unable-to-transport' THEN AttributeValue 
				 WHEN WorkflowType = 'pickup' AND Outcome = 'failed-no-goods-to-go' THEN AttributeValue 
				 WHEN WorkflowType = 'pickup' AND Outcome = 'failed-goods-not-ready' THEN AttributeValue  			 
				 WHEN WorkflowType = 'pickup' AND Outcome = 'failed-defer' THEN AttributeValue
				 ELSE NULL END 
			AS ExceptionReason,
			NULL AS AlternateBarcode,
			NULL AS [RedeliveryCard],
			CASE WHEN WorkflowType = 'offboard' AND Outcome = 'offboard-popshop' THEN AttributeValue 
				 WHEN WorkflowType = 'offboard' AND Outcome = 'offboard-popstation' THEN AttributeValue 
				 --CH #153	
				 WHEN WorkflowType = 'transfer' AND Outcome = 'transfer-query-freight' THEN IIF (charindex('DR', AttributeValue)>0,SUBSTRING(AttributeValue,3,20) , AttributeValue) 
				 WHEN WorkflowType = 'transfer' AND Outcome = 'transfer-other' THEN IIF (charindex('DR', AttributeValue)>0,SUBSTRING(AttributeValue,3,20) , AttributeValue) 
				 WHEN WorkflowType = 'transfer' AND Outcome = 'transfer-driver' THEN IIF (charindex('DR', AttributeValue)>0,SUBSTRING(AttributeValue,3,20) , AttributeValue)  			 
				 			 
				 ELSE NULL END 
			AS [DLB],
			NULL AS [URL],
			[SigneeName] AS [PODName],
			NULL AS PODImageID,
			NULL AS ImageType,
			NowGoWorkflowCompletionID,
			NowGoConsignmentID,
			NowGoSubjectArticleID,
			CASE WHEN WorkflowType = 'pickup' AND ([LabelNumber] like 'CPLLS%' OR [LabelNumber] like 'LLS%') THEN BookingID ELSE NULL END As [BookingID]
		FROM [dbo].[tbl_TrackingEvent] WITH (NOLOCK)
		WHERE [WorkflowType] != 'deliver' AND [Outcome] != 'delivered'
		AND IsProcessed = 0

		UNION

		-- Failed Delivery Tracking Events
		SELECT
			TRK.EventID,
			TRK.[Labelnumber] AS [TrackingNumber], 
			NULL AS [ConsignmentNumber],
			[WorkflowType],
			[Outcome],
			CONVERT(VARCHAR, EventDatetime, 120) AS [EventDateTime], 
			CASE 
				 WHEN Outcome = 'failed-not-home-or-closed' THEN 'Attempted Delivery'
				  WHEN Outcome = 'failed-closed' THEN 'Attempted Delivery'
				 WHEN Outcome = 'failed-not-home' THEN 'Attempted Delivery'
				 WHEN Outcome = 'failed-other' THEN 'Attempted Delivery' 
				 WHEN Outcome = 'failed-incomplete' THEN 'Attempted Delivery' 
				 WHEN Outcome = 'failed-unsafe-to-leave' THEN 'Attempted Delivery'
				 WHEN Outcome = 'failed-refused-by-customer' THEN 'Attempted Delivery'
				 WHEN Outcome = 'failed-damaged' THEN 'Attempted Delivery'
				 --Ch # 439
				 WHEN Outcome = 'failed-alcohol-suspected-intoxication' THEN 'Attempted Delivery'
				 WHEN Outcome = 'failed-alcohol-unacceptable-id' THEN 'Attempted Delivery'
				 WHEN Outcome = 'failed-alcohol-risk-of-supply-to-minor' THEN 'Attempted Delivery'
				 ELSE Outcome
			END AS ScanEvent, 
			--Change made by AH - 21/04/2020 for DeliverE Agent only
			CASE WHEN rtrim(ltrim(driverextref))= 'delivere-delegate-driver' THEN '9407' ELSE [DriverRunNumber] END as [DriverRunNumber],
			CASE WHEN rtrim(ltrim(driverextref))= 'delivere-delegate-driver' THEN 'sydney' ELSE [Branch] END as [Branch],
			
			--CH#229
			CASE WHEN Outcome = 'failed-not-home-or-closed' THEN 'Card Left - Closed redeliver next cycle'
				WHEN Outcome = 'failed-closed' THEN 'Closed' 
				WHEN Outcome = 'failed-not-home' THEN 'Card Left - Not Home'  
				 WHEN Outcome = 'failed-unsafe-to-leave' THEN 'Card Left - Unsafe to Leave'
				 --CH#184 & CH#229
				 WHEN Outcome = 'failed-incomplete' THEN 'Incomplete Consignment'
				 --Ch # 439
				 WHEN Outcome = 'failed-alcohol-suspected-intoxication' THEN 'Unsafe Alcohol Delivery: Intoxication suspected'
				 WHEN Outcome = 'failed-alcohol-unacceptable-id' THEN 'Unsafe Alcohol Delivery: Unacceptable ID'
				 WHEN Outcome = 'failed-alcohol-risk-of-supply-to-minor' THEN 'Unsafe Alcohol Delivery: Risk of supply to a minor'
				 ELSE NULL END 
			AS [ExceptionReason],
			NULL AS AlternateBarcode,
			IIF(Outcome = 'failed-not-home-or-closed'  OR Outcome = 'failed-not-home' OR Outcome = 'failed-unsafe-to-leave', [AttributeValue], NULL) AS [RedeliveryCard],
			NULL AS [DLB],
			NULL AS [URL],
			[SigneeName] AS [PODName],
			-- CH # 324 - AH - 28/10/2020
			NULL AS PODImageID,
			NULL AS ImageType,
			NowGoWorkflowCompletionID,
			NowGoConsignmentID,
			NowGoSubjectArticleID,
			NULL AS BookingID
		FROM [dbo].[tbl_TrackingEvent] TRK WITH (NOLOCK)	
		-- CH # 324 - AH - 28/10/2020
		WHERE  (WorkflowType = 'deliver' AND [Outcome] != 'delivered' AND [Outcome] <> 'delivered-alcohol' )
		AND IsProcessed = 0

		UNION

		-- Delivered Tracking Events with POD Signature
		SELECT
			TRK.EventID,
			TRK.[Labelnumber] AS [TrackingNumber], 
			NULL AS [ConsignmentNumber],
			[WorkflowType],
			[Outcome]  ,
			CONVERT(VARCHAR, EventDatetime, 120) AS [EventDateTime], 
			-- CH # 324 - AH - 28/10/2020
			CASE WHEN [outcome]='delivered-alcohol' THEN 'delivered' else [Outcome] END  AS ScanEvent, 
			--Change made by AH - 21/04/2020 for DeliverE Agent only
			CASE WHEN rtrim(ltrim(driverextref))= 'delivere-delegate-driver' THEN '9407' ELSE [DriverRunNumber] END as [DriverRunNumber],
			CASE WHEN rtrim(ltrim(driverextref))= 'delivere-delegate-driver' THEN 'sydney' ELSE [Branch] END as [Branch],
			
			NULL AS [ExceptionReason],
			NULL AS AlternateBarcode,
			NULL AS [RedeliveryCard],
			NULL AS [DLB],
			NULL AS [URL],
			[SigneeName] AS [PODName],
			PODImageID,
			ImageType,
			NowGoWorkflowCompletionID,
			NowGoConsignmentID,
			NowGoSubjectArticleID,
			NULL AS BookingID
		FROM [dbo].[tbl_TrackingEvent] TRK WITH (NOLOCK)
		INNER JOIN [tbl_TrackingEvent_PODImage] IMG WITH (NOLOCK)
		ON TRK.EventID = IMG.EventID
		WHERE
		--CH# 324
		([WorkflowType] = 'deliver' AND [Outcome] = 'delivered' AND (IMG.IsDownloaded = 1 AND IMG.ImageDetail LIKE 'Signature%'))
		AND IsProcessed = 0

		UNION

		-- Delivered Tracking Events Without POD Image
		SELECT
			TRK.EventID,
			TRK.[Labelnumber] AS [TrackingNumber], 
			NULL AS [ConsignmentNumber],
			[WorkflowType],
			[Outcome],
			CONVERT(VARCHAR, EventDatetime, 120) AS [EventDateTime], 
			-- CH # 324 - AH - 28/10/2020
			CASE WHEN [outcome]='delivered-alcohol' THEN 'delivered' else [Outcome] END  AS ScanEvent, 
			--Change made by AH - 21/04/2020 for DeliverE Agent only
			CASE WHEN rtrim(ltrim(driverextref))= 'delivere-delegate-driver' THEN '9407' ELSE [DriverRunNumber] END as [DriverRunNumber],
			CASE WHEN rtrim(ltrim(driverextref))= 'delivere-delegate-driver' THEN 'sydney' ELSE [Branch] END as [Branch],
			
			--CH # 168 - Change ATL Display Message
			CASE WHEN Attribute LIKE 'ATL%' THEN 'ATL'
				 WHEN Attribute LIKE 'DLB%' THEN 'Delivery Barcode - ' + AttributeValue			 
				 ELSE NULL END 
			AS [ExceptionReason],
			NULL AS AlternateBarcode,
			NULL AS [RedeliveryCard],
			IIF([Attribute] = 'DLB', [AttributeValue], NULL) AS [DLB],
			PODImageID AS [URL], -----------------------------Updated by PV
			[SigneeName] AS [PODName],
			NULL AS PODImageID,
			ImageType AS ImageType,
			NowGoWorkflowCompletionID,
			NowGoConsignmentID,
			NowGoSubjectArticleID,
			NULL AS BookingID
		FROM [dbo].[tbl_TrackingEvent] TRK WITH (NOLOCK)
		INNER JOIN [tbl_TrackingEvent_PODImage] IMG WITH (NOLOCK)
		ON TRK.EventID = IMG.EventID
		WHERE
		--CH# 324
		([WorkflowType] = 'deliver' AND [Outcome] = 'delivered'  AND (IMG.NowGoImageID IS NULL OR IMG.ImageDetail NOT LIKE 'Signature%'))
		AND IsProcessed = 0 
		
		UNION

		--Delivered-Alcohol Tracking Events with POD Signature
		SELECT
			TRK.EventID,
			TRK.[Labelnumber] AS [TrackingNumber], 
			NULL AS [ConsignmentNumber],
			[WorkflowType],
			[Outcome],
			CONVERT(VARCHAR, EventDatetime, 120) AS [EventDateTime], 
			CASE 
				 WHEN Outcome = 'failed-not-home-or-closed' THEN 'Attempted Delivery'
				  WHEN Outcome = 'failed-closed' THEN 'Attempted Delivery'
				 WHEN Outcome = 'failed-not-home' THEN 'Attempted Delivery'
				 WHEN Outcome = 'failed-other' THEN 'Attempted Delivery' 
				 WHEN Outcome = 'failed-incomplete' THEN 'Attempted Delivery' 
				 WHEN Outcome = 'failed-unsafe-to-leave' THEN 'Attempted Delivery'
				 WHEN Outcome = 'failed-refused-by-customer' THEN 'Attempted Delivery'
				 WHEN Outcome = 'failed-damaged' THEN 'Attempted Delivery'
				 --Ch # 439
				 WHEN Outcome = 'failed-alcohol-suspected-intoxication' THEN 'Attempted Delivery'
				 WHEN Outcome = 'failed-alcohol-unacceptable-id' THEN 'Attempted Delivery'
				 WHEN Outcome = 'failed-alcohol-risk-of-supply-to-minor' THEN 'Attempted Delivery'
				 -- CH # 324 - AH - 28/10/2020	
				 WHEN [outcome]='delivered-alcohol' THEN 'delivered'			
				 ELSE Outcome
			END AS ScanEvent, 
			--Change made by AH - 21/04/2020 for DeliverE Agent only
			CASE WHEN rtrim(ltrim(driverextref))= 'delivere-delegate-driver' THEN '9407' ELSE [DriverRunNumber] END as [DriverRunNumber],
			CASE WHEN rtrim(ltrim(driverextref))= 'delivere-delegate-driver' THEN 'sydney' ELSE [Branch] END as [Branch],
			
			--CH#229
			CASE WHEN Outcome = 'failed-not-home-or-closed' THEN 'Card Left - Closed redeliver next cycle'
				WHEN Outcome = 'failed-closed' THEN 'Closed' 
				WHEN Outcome = 'failed-not-home' THEN 'Card Left - Not Home'  
				 WHEN Outcome = 'failed-unsafe-to-leave' THEN 'Card Left - Unsafe to Leave'
				 --CH#184 & CH#229
				 WHEN Outcome = 'failed-incomplete' THEN 'Incomplete Consignment'
				 WHEN Outcome = 'failed-other' THEN AttributeValue
				 WHEN Outcome = 'failed-refused-by-customer' THEN 'Return to Sender - Refused Delivery '
				 --Ch # 439
				 WHEN Outcome = 'failed-alcohol-suspected-intoxication' THEN 'Unsafe Alcohol Delivery: Intoxication suspected'
				 WHEN Outcome = 'failed-alcohol-unacceptable-id' THEN 'Unsafe Alcohol Delivery: Unacceptable ID'
				 WHEN Outcome = 'failed-alcohol-risk-of-supply-to-minor' THEN 'Unsafe Alcohol Delivery: Risk of supply to a minor'
				 
				 ELSE Outcome END 
			AS [ExceptionReason],
			NULL AS AlternateBarcode,
			IIF(Outcome = 'failed-not-home-or-closed'  OR Outcome = 'failed-not-home' OR Outcome = 'failed-unsafe-to-leave', [AttributeValue], NULL) AS [RedeliveryCard],
			NULL AS [DLB],
			NULL AS [URL],
			[SigneeName] AS [PODName],
			-- CH # 324 - AH - 28/10/2020
			CASE WHEN [outcome]='delivered-alcohol' THEN IMG.PODImageID ELSE NULL END AS PODImageID,
			CASE WHEN [outcome]='delivered-alcohol' THEN IMG.ImageType  ELSE NULL END AS ImageType,
			NowGoWorkflowCompletionID,
			NowGoConsignmentID,
			NowGoSubjectArticleID,
			NULL AS BookingID
		FROM [dbo].[tbl_TrackingEvent] TRK WITH (NOLOCK)	
		-- CH # 324 - AH - 28/10/2020
		INNER JOIN [tbl_TrackingEvent_PODImage] IMG WITH (NOLOCK) ON TRK.EventID = IMG.EventID	
		WHERE  (WorkflowType = 'deliver' AND [outcome]='delivered-alcohol' AND (IMG.IsDownloaded = 1 AND IMG.ImageDetail LIKE 'Signature%')) --OR (IMG.NowGoImageID IS NULL OR IMG.ImageDetail NOT LIKE 'Signature%'))
		AND IsProcessed = 0 
		
		UNION 

		-- Delivered-Alcohol Tracking Events Without POD Image
		SELECT
			TRK.EventID,
			TRK.[Labelnumber] AS [TrackingNumber], 
			NULL AS [ConsignmentNumber],
			[WorkflowType],
			[Outcome],
			CONVERT(VARCHAR, EventDatetime, 120) AS [EventDateTime], 
			-- CH # 324 - AH - 28/10/2020
			CASE WHEN [outcome]='delivered-alcohol' THEN 'delivered' else [Outcome] END  AS ScanEvent, 
			--Change made by AH - 21/04/2020 for DeliverE Agent only
			CASE WHEN rtrim(ltrim(driverextref))= 'delivere-delegate-driver' THEN '9407' ELSE [DriverRunNumber] END as [DriverRunNumber],
			CASE WHEN rtrim(ltrim(driverextref))= 'delivere-delegate-driver' THEN 'sydney' ELSE [Branch] END as [Branch],
			
			--CH # 168 - Change ATL Display Message
			CASE WHEN Attribute LIKE 'ATL%' THEN 'ATL'
				 WHEN Attribute LIKE 'DLB%' THEN 'Delivery Barcode - ' + AttributeValue			 
				 ELSE NULL END 
			AS [ExceptionReason],
			NULL AS AlternateBarcode,
			NULL AS [RedeliveryCard],
			IIF([Attribute] = 'DLB', [AttributeValue], NULL) AS [DLB],
			PODImageID AS [URL], --Update by PV
			[SigneeName] AS [PODName],
			NULL AS PODImageID,
			ImageType AS ImageType,
			NowGoWorkflowCompletionID,
			NowGoConsignmentID,
			NowGoSubjectArticleID,
			NULL AS BookingID
		FROM [dbo].[tbl_TrackingEvent] TRK WITH (NOLOCK)
		INNER JOIN [tbl_TrackingEvent_PODImage] IMG WITH (NOLOCK)
		ON TRK.EventID = IMG.EventID
		WHERE
		--CH# 324
		([WorkflowType] = 'deliver' AND [Outcome] = 'delivered-alcohol'  AND (IMG.NowGoImageID IS NULL OR IMG.ImageDetail NOT LIKE 'Signature%'))
		AND IsProcessed = 0 

		) A
END


GO
