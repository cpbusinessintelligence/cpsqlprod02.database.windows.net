SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_TrackingEvent_Update_Reprocess_Flag]
(
        @rowid bigint
)
AS
BEGIN
    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	UPDATE [dbo].[tbl_TrackingEvent_FailedTransactions] SET IsProcessed = 1 where rowid = @rowid

END
GO
