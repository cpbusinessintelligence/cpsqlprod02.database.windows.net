SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





CREATE FUNCTION [dbo].[fn_GET_ATTRIBUTES_Bkup_20210215_AH] 
(
	@JSON NVARCHAR(MAX),
	@WorkflowType VARCHAR(50),
	@Outcome VARCHAR(50)
)
RETURNS @tbl_Attribute TABLE(Attribute VARCHAR(50), AttributeValue VARCHAR(100))

AS
BEGIN
	DECLARE @Attribute VARCHAR(50) = NULL;
	DECLARE @AttributeValue VARCHAR(100) = NULL;
	DECLARE @Data VARCHAR(50) = NULL;

	IF @WorkflowType = 'offboard' 
	BEGIN
		IF @Outcome = 'offboard-popshop'
		BEGIN
			SET @Attribute = 'Left at PopShop';
			SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.custom_data.barcode'));
		END
		ELSE IF @Outcome = 'offboard-popstation'
		BEGIN
			SET @Attribute = 'Left at PopStation';
			SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.custom_data.barcode'));
		END
	END

	IF @WorkflowType = 'deliver' 
	BEGIN
		IF (@Outcome = 'delivered' Or @Outcome = 'delivered-alcohol')
		BEGIN
			IF (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.signature_image.label')) = 'Signature'
			BEGIN
				SET @Attribute = 'Signature';
				SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.signature_image.id'));
			END
			ELSE IF (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.signature_image')) IS NULL AND (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.attachments[0].label')) = 'ATL Photo' AND (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.custom_data.location_code')) IS NULL
			BEGIN
				SET @Attribute = 'ATL With Photo';
				SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.attachments[0].id'));
			END
			ELSE IF (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.signature_image')) IS NULL AND (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.attachments[0].label')) IS NULL AND (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.custom_data.location_code')) IS NULL
			BEGIN
				SET @Attribute = 'ATL Without Photo';
				SET @AttributeValue = NULL;
			END
			ELSE IF (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.custom_data.location_code')) IS NOT NULL 
			BEGIN
				SET @Attribute = 'DLB';
				SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.custom_data.location_code'));
			END
		END

		ELSE IF @Outcome = 'failed-other'
		BEGIN
			SET @Attribute = 'Other';
			SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.custom_data.other_failure_reason'));
		END

		


		ELSE IF @Outcome = 'failed-unsafe-to-leave'
		BEGIN
			SET @Data = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.custom_data.futile_action'));

			IF @Data = 'popshop'			 
			BEGIN
				SET @Attribute = 'Taken to PopShop/Left redelivery card';				
			END
			ELSE IF @Data = 'popstation'
			BEGIN
				SET @Attribute = 'Taken to PopStation/Left redelivery card';				
			END
			ELSE IF @Data = 'depot'
			BEGIN
				SET @Attribute = 'Taken to Depot/Left redelivery card';				
			END

			SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.custom_data.nhcl_barcode'));
		END

		ELSE IF @Outcome = 'failed-not-home-or-closed'
		BEGIN
			SET @Data = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.custom_data.futile_action'));

			IF @Data = 'popshop'			 
			BEGIN
				SET @Attribute = 'Taken to PopShop/Left redelivery card';				
			END
			ELSE IF @Data = 'popstation'
			BEGIN
				SET @Attribute = 'Taken to PopStation/Left redelivery card';				
			END
			ELSE IF @Data = 'depot'
			BEGIN
				SET @Attribute = 'Taken to Depot/Left redelivery card';				
			END

			SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.custom_data.nhcl_barcode'));
		END

		ELSE IF @Outcome = 'failed-refused-by-customer'
		BEGIN
			SET @Attribute = 'Failed';
			SET @AttributeValue = 'Refused by customer';
		END

		ELSE IF @Outcome = 'failed-incomplete'
		BEGIN
			SET @Attribute = 'Failed';
			SET @AttributeValue = 'Incomplete';
		END

		ELSE IF @Outcome = 'failed-damaged'
		BEGIN
			SET @Attribute = 'Failed';
			SET @AttributeValue = 'Damaged';
		END		
	END

	IF @WorkflowType = 'pickup'
	BEGIN
		IF @Outcome = 'pickup'
		BEGIN
			IF (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.custom_data.dlb')) IS NOT NULL 
			BEGIN
				SET @Attribute = 'DLB';
				SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.custom_data.dlb'));
			END
			ELSE
			BEGIN
				SET @Attribute = 'Standard';
				SET @AttributeValue = NULL;
			END
		END
		ELSE IF @Outcome = 'failed-pickup'
		BEGIN
			SET @Attribute = 'Failed';
			SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.custom_data.other_failure_reason'));
		END
		ELSE IF @Outcome = 'failed-defer'
		BEGIN
			SET @Attribute = 'Failed';
			SET @AttributeValue = 'Defer';
		END
		ELSE IF @Outcome = 'failed-wrong-job'
		BEGIN
			SET @Attribute = 'Failed';
			SET @AttributeValue = 'Wrong job';
		END
		ELSE IF @Outcome = 'failed-no-access-available'
		BEGIN
			SET @Attribute = 'Failed';
			SET @AttributeValue = 'No access available';
		END
		ELSE IF @Outcome = 'failed-closed'
		BEGIN
			SET @Attribute = 'Failed';
			SET @AttributeValue = 'Closed';
		END
		ELSE IF @Outcome = 'failed-unable-to-transport'
		BEGIN
			SET @Attribute = 'Failed';
			SET @AttributeValue = 'Unable to transport';
		END
		ELSE IF @Outcome = 'failed-no-goods-to-go'
		BEGIN
			SET @Attribute = 'Failed';
			SET @AttributeValue = 'No goods to go';
		END
		ELSE IF @Outcome = 'failed-goods-not-ready'
		BEGIN
			SET @Attribute = 'Failed';
			SET @AttributeValue = 'Goods not ready';
		END
	END
	-- Added as per CR #153 - 06/06/2019 - Anand
	IF @WorkflowType = 'transfer'
	BEGIN
		 IF @Outcome = 'transfer-other'

			BEGIN
				SET @Attribute = 'DLB';
				SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.custom_data.barcode'));
			END
		 ELSE IF @Outcome = 'transfer-driver'

			BEGIN
				SET @Attribute = 'DLB';
				SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.custom_data.barcode'));
			END
		 ELSE IF @Outcome = 'transfer-query-freight'

			BEGIN
				SET @Attribute = 'DLB';
				SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.custom_data.barcode'));
			END
	END
	INSERT INTO @tbl_Attribute VALUES (@Attribute, @AttributeValue);
	RETURN 
END
GO
