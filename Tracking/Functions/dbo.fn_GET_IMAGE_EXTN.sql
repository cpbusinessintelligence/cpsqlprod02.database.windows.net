SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_GET_IMAGE_EXTN] 
(
	@ImageType VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
BEGIN
	
	DECLARE @ImageExt VARCHAR(10) = NULL;

	BEGIN
		IF(@ImageType LIKE '%jpeg')
			SET @ImageExt = '.jpeg';
		ELSE IF (@ImageType LIKE '%jpg')
			SET @ImageExt = '.jpg';
		ELSE IF (@ImageType LIKE '%png')
			SET @ImageExt = '.png';
		ELSE 
			SET @ImageExt = '.png';
	END
	RETURN @ImageExt;
END
GO
