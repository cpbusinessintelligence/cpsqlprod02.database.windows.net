SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_GET_DRIVER_RUN_NUMBER] 
(
	@DRIVER_EXT_REF VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
/*
CHANGE HISTORY
----------------------------------------------------------------------------------------------------------------------------------------------------------------
CR # 202 - Anand - 23/09/2019 - Change Driver Ext Ref, if _01 for an alias driver appears store in database as is but strip off _01 and send as is 

----------------------------------------------------------------------------------------------------------------------------------------------------------------

*/ 
BEGIN

	DECLARE @DRIVER_NUMBER AS VARCHAR(50);

	SET @DRIVER_EXT_REF = TRIM(@DRIVER_EXT_REF);

	if CHARINDEX('_',@DRIVER_EXT_REF) > 0
	BEGIN
	--CR # 202
		SET @DRIVER_NUMBER = IIF(@DRIVER_EXT_REF IS NOT NULL AND LEN(@DRIVER_EXT_REF)>6 , TRY_CONVERT(VARCHAR, TRY_CONVERT(INT, SUBSTRING(@DRIVER_EXT_REF, 7, LEN(@DRIVER_EXT_REF)-8))), NULL)
	END
	ELSE
	BEGIN
		SET @DRIVER_NUMBER = IIF(@DRIVER_EXT_REF IS NOT NULL AND LEN(@DRIVER_EXT_REF)>6 , TRY_CONVERT(VARCHAR, TRY_CONVERT(INT, SUBSTRING(@DRIVER_EXT_REF, 7, LEN(@DRIVER_EXT_REF)-6))), NULL)
	END
	RETURN @DRIVER_NUMBER;

END
GO
